//  Валидация заполнения полей формы

/*  Изменения по замечаниям:
    События к контролам навешены через обраточик событий
*/

(function(){
    "use strict";

    document.addEventListener("DOMContentLoaded", init, false);

    function init(){
    
        var isChecked = false;

        var btnSubmit = document.getElementById("btnSubmit");
        
        var firstName = document.getElementById("first-name");
        var lastName = document.getElementById("last-name");
        var country = document.getElementById("select-country");
        var subscribe = document.getElementById("checkbox-subscribe");
        var educations = document.getElementsByName("radio-education");
        var about = document.getElementById("textarea-about");
        
        var controlsForCheck = [
            firstName,
            lastName,
            country,
            about
        ];

        //  Установка обработчкиков событий

        btnSubmit.addEventListener("click", saveUser);
        btnReset.addEventListener("click", resetControls);

        controlsForCheck.forEach(function(obj) {
            obj.addEventListener("input", invalidToValid)
        }, this);
        
        //  Проверка валидации и создание пользователя
        function saveUser(){
            var isValid = true;

            //  Проверка заданных полей.
            controlsForCheck.forEach(function(obj) {
                if (!setValidStyle(obj))
                    isValid = false;
            }, this);

            //  Если все поля прошли валидацию, то выполняется создание объекта "user".
            if (isValid){

                var education;
                educations.forEach(function(obj){
                    if (obj.checked){
                        education = obj;
                    }
                }, this);
                
                var user = {
                    firstName: firstName.value,
                    lastName: lastName.value,
                    country: country.value,
                    subscribe: subscribe.checked,
                    education: education.value,
                    about: about.value
                }

                console.log(user);
            }

            /*  Отметка о том, что была выполнена проверка,
                для изменения стиля отображения поля при изменении его валидации.
            */
            isChecked = true;
        }

        //  Установка стиля поля в зависимости от валидации.
        function setValidStyle(obj){
            if (getIsValid(obj.value)){
                obj.className = "valid control";
                return true;
            }
            else{
                obj.className = "invalid control";
                return false;
            }
        }

        //  Условия валидации.
        function getIsValid(obj){
            if (obj !== "" && obj !== "country-change")
                return true;
            else
                return false;
        }

        /*  Изменение стиля отображения поля при изменении его валидации.
            Не выполняется до нажатия кнопки "Сохранить".
        */
        function invalidToValid(){
            if (isChecked)
                setValidStyle(this);
        }

        //  Сброс стилей
        function resetControls(){
            isChecked = false;

            controlsForCheck.forEach(function(obj) {
                obj.className = "default control";
            }, this);
        }
    }
})();