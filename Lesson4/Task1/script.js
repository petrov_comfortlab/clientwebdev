/*  Изменения по замечанию:
    Код перенесён из глобальной области видимости.
*/

//  Создание списков с рецептами

(function(obj){

    obj.recipe1 = [
        "белый хлеб (400 г)",
        "масло оливковое или подсолнечное (3 столовые ложки)",
        "чеснок (2 зубчика)",
        "сыр российский (100 г)",
        "соль"
    ];
    obj.recipe2 = [
        "буханка черного хлеба (1 шт.)",
        "масло растительное (2 столовые ложки)",
        "прованские травы",
        "соль"
    ];

    obj.createRecipe = function(recipe){
        
        var ul = document.createElement('ul');
        
        recipe.forEach(function(element) {
            var li = document.createElement('li');
            li.innerHTML = element;
            ul.appendChild(li);
        }, this);
        
        document.body.appendChild(ul);
    }
})(this.recipes = {});

//  Скрытие рекламного баннера

(function(){

    hideBanner = function(banner){
        function deleteBanner() {
            document.body.removeChild(banner);
        }
        
        banner.className = "banner-hide";
        setTimeout(deleteBanner, 1000);
    }

})();