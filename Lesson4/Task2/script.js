//  Изменить цвет ячейки с чётным номером сроки и столбца

/*  Изменения по замечаниям:
    События к ячейкам таблицы навешены через обраточик событий
*/

(function(){
    "use strict";

    document.addEventListener("DOMContentLoaded", init, false);

    function init(){
        
        var elementCells = document.getElementsByTagName("TD");
        var currentClass;

        for (var i = 0; i < elementCells.length; i++) {
            var obj = elementCells[i];
            
            if (obj.className.indexOf("cell-element") !== -1){
                obj.addEventListener("mouseover", changeBackgroundCell, false);
                obj.addEventListener("mouseout", setDefaultBackgroundCell, false);
            }
        }(i);

        function changeBackgroundCell(){
            currentClass = this.className;
            
            var cellIndex = this.cellIndex + 1;
            var rowIndex = this.parentNode.rowIndex;

            if (cellIndex % 2 === 1 && rowIndex % 2 === 1)
                this.className = "cell-element-select";
        }

        function setDefaultBackgroundCell(){
            if (this.className === "cell-element-select")
                this.className = currentClass;
        }
    }
})();
