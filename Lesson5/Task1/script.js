//  Создание списков с рецептами

(function(){

var recipe1 = [
    "белый хлеб (400 г)",
    "масло оливковое или подсолнечное (3 столовые ложки)",
    "чеснок (2 зубчика)",
    "сыр российский (100 г)",
    "соль"
];

var recipe2 = [  
    "буханка черного хлеба (1 шт.)",
    "масло растительное (2 столовые ложки)",
    "прованские травы",
    "соль"
];

/*  Изменение по заданию к 5-му занятию:

*/
var words = [
    "масло",
    "",
];

function createUl(recipe){
    
    var ul = document.createElement('ul');
    
    recipe.forEach(function(element) {
        var li = document.createElement('li');
        li.innerHTML = element;
        ul.appendChild(li);
    }, this);
    
    document.body.appendChild(ul);
}

//  Скрытие рекламного баннера

hideBanner = function(banner){
    function deleteBanner() {
        document.body.removeChild(banner);
    }
    
    banner.className = "banner-hide";
    setTimeout(deleteBanner, 1000);
}

})();