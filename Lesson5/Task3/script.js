//  Валидация заполнения полей формы

(function(){
    
    var isChecked = false;

    var controlsForCheck;

    saveUser = function(){

        var firstName = document.getElementById("first-name");
        var lastName = document.getElementById("last-name");
        var country = document.getElementById("select-country");
        var subscribe = document.getElementById("checkbox-subscribe");
        var educations = document.getElementsByName("radio-education");
        var about = document.getElementById("textarea-about");
        
        controlsForCheck = [
            firstName,
            lastName,
            country,
            about];
            
        var isValid = true;

        //  Проверка заданных полей.
        controlsForCheck.forEach(function(obj) {
            if (!setValidStyle(obj))
                isValid = false;
        }, this);
        
        //  Если все поля прошли валидацию, то выполняется создание объекта "user".
        if (isValid){

            var education;
            educations.forEach(function(obj){
                if (obj.checked){
                    education = obj;
                }
            }, this);
            
            var user = {
                firstName: firstName.value,
                lastName: lastName.value,
                country: country.value,
                subscribe: subscribe.checked,
                education: education.value,
                about: about.value
            }

            console.log(user);
        }

        /*  Отметка о том, что была выполнена проверка,
            для изменения стиля отображения поля при изменении его валидации.
        */
        isChecked = true;
    }

    /*  Изменение стиля отображения поля при изменении его валидации.
        Не выполняется до нажатия кнопки "Сохранить".
    */
    invalidToValid = function(obj){
        if (isChecked)
            setValidStyle(obj);
    }

    //  Установка стиля поля в зависимости от валидации.
    setValidStyle = function(obj){
        if (getIsValid(obj.value)){
            obj.className = "valid control";
            return true;
        }
        else{
            obj.className = "invalid control";
            return false;
        }
    }

    //  Условия валидации.
    getIsValid = function(obj){
        if (obj !== "" && obj !== "country-change")
            return true;
        else
            return false;
    }

    //  Сброс стилей
    resetControls = function(){
        isChecked = false;

        controlsForCheck.forEach(function(obj) {
            obj.className = "default control";
        }, this);
    }
})();