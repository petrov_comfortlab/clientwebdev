//  Изменить цвет ячейки с чётным номером сроки и столбца

var currentClass = null;
var activeCell = null;

function changeBackgroundTable(e)
{
    var evt = window.event || e;
    var obj = evt.srcElement || evt.target;
    
    if (obj.tagName == 'TABLE')
        return;

    //  Так как таблица завёрнута в "div", то будет выполнен перебор родителей элемента "obj" в границах таблицы
    while(obj.tagName != 'DIV')
    {
        /*  Каждая ячейка химического элемента основной таблицы является таблицей.
            Получение ячейки класса "cell-element" путём перебора родителей объекта.
        */
        if (obj.className.indexOf("cell-element") == -1)
        {
            obj = obj.parentNode;
        }
        else
        {
            var cell = obj;
            
            /*  При выходе из перекрашенной ячейки необходимо вернуть ей начальный цвет
                и сбробсить активную ячейку.
            */
            if (activeCell != null && currentClass != null)
            {
                activeCell.className = currentClass;
                activeCell = null;
            }
            
            var cellIndex = cell.cellIndex; 
            var rowIndex = cell.parentNode.rowIndex;

            if (cellIndex % 2 == 0 && rowIndex % 2 == 1)
            {
                /*  Запись первоначального класса ячейки для его возврата при выходе из ячейки.
                    При этом предотвращается переписывание начального класса ячейки при движении мышки по ячейке.
                */
                if (cell.className != "cell-element-select")
                    currentClass = cell.className;
                
                /*  Присвоение нового класса ячейке.
                    Ячейка записывается в переменную "activeCell" для того, чтобы можно было ей идентифицировать
                    при выходе из неё.
                */
                activeCell = cell;
                activeCell.className = "cell-element-select";
            }

            break;
        }
    }    
}

/*  Вариант 2
    Методы changeBackgroungCell и setDefaultBackgroundCell можно применить для
    обработки событий ячейки таблицы "onmouseover" и "onmouseout" соответственно.
    Минусом этого метода является то, что надо прописывать обработчики для каждой ячейки.
*/

var currentClass;

changeBackgroundCell = function(cell){
    currentClass = cell.className;
    var cellIndex = cell.cellIndex + 1;
    var rowIndex = cell.parentNode.rowIndex;

    if (cellIndex % 2 == 1 && rowIndex % 2 == 1)
        cell.className = "cell-element-select";
}

setDefaultBackgroundCell = function(cell){
    if (cell.className == "cell-element-select")
        cell.className = currentClass;
}
